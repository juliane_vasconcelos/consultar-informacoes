package br.com.galgo.testes.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.consultar.arquivo.ConsultarExtratoArquivo;
import br.com.galgo.consultar.arquivo.ConsultarInformacaoAnbimaArquivo;
import br.com.galgo.consultar.arquivo.ConsultarPlCotaArquivo;
import br.com.galgo.consultar.arquivo.ConsultarPosicaoAtivosArquivo;
import br.com.galgo.consultar.portal.ConsultarExtratoPortal;
import br.com.galgo.consultar.portal.ConsultarInformacaoAnbimaPortal;
import br.com.galgo.consultar.portal.ConsultarPlCotaPortal;
import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({ ConsultarPlCotaPortal.class,//
		ConsultarInformacaoAnbimaPortal.class,//
		ConsultarExtratoPortal.class, //
		ConsultarPlCotaArquivo.class,//
		ConsultarInformacaoAnbimaArquivo.class,//
		ConsultarExtratoArquivo.class,//
		ConsultarPosicaoAtivosArquivo.class //
})
public class SuiteConsulta {

}
