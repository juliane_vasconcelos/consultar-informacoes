package br.com.galgo.consultar;

import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaExtrato;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaPLCota;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaPosicao;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;

public class ConsultarInformacoes extends TelaGalgo {

	private Teste teste;
	private String caminhoMassaDadosFundos;

	public void consultarInformacao(Teste teste) throws Exception {
		this.teste = teste;
		Ambiente ambiente = teste.getAmbiente();

		caminhoMassaDadosFundos = MassaDados.fromAmbiente(ambiente,
				ConstantesTestes.DESC_MASSA_DADOS_TRANSF).getPath();

		String login = ArquivoUtils.getLogin(teste, caminhoMassaDadosFundos);
		String senha = ArquivoUtils.getSenha(teste, caminhoMassaDadosFundos);
		Usuario usuario = new Usuario(login, senha, ambiente);

		consultarInformacao(usuario);
	}

	public void consultarInformacao(Usuario usuario) throws Exception {
		Ambiente ambiente = teste.getAmbiente();
		Servico servico = teste.getServico();
		Canal canal = teste.getCanal();
		int qtdReteste = teste.getQtdReteste();

		TelaGalgo.abrirBrowser(ambiente.getUrl());
		TelaLogin telaLogin = new TelaLogin();
		telaLogin.loginAs(usuario);
		int qtdExecucoes = 1;
		while (qtdExecucoes <= qtdReteste) {

			String codigoSTI = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, 1, qtdExecucoes);
			String dataBase = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, 2, qtdExecucoes);

			String codigoSTICotista = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, false, 3, qtdExecucoes);

			consultar(usuario, servico, canal, dataBase, codigoSTI,
					codigoSTICotista);
			qtdExecucoes++;
		}

	}

	private void consultar(Usuario usuario, Servico servico, Canal canal,
			String dataFiltro, String codigoSTI, String codigoSTICotista) {
		if (Servico.EXTRATO == servico) {
			consultaExtrato(usuario, canal, dataFiltro, codigoSTI,
					codigoSTICotista);
		} else if (Servico.PL_COTA == servico) {
			consultarPlCota(usuario, canal, dataFiltro, codigoSTI);
		} else if (Servico.INFO_ANBIMA == servico) {
			consultaInfoAnbima(usuario, canal, dataFiltro, codigoSTI);
		} else if (Servico.POSICAO_ATIVOS == servico) {
			consultaPosicao(usuario, dataFiltro, codigoSTI);
		}
	}

	private void consultaExtrato(Usuario usuario, Canal canal,
			String dataFiltro, String codigoSTI, String codigoSTICotista) {
		TelaHome telaHome = new TelaHome(usuario);
		TelaConsultaExtrato telaConsultaExtrato = (TelaConsultaExtrato) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_EXTRATO);
		if (Canal.PORTAL == canal) {
			telaConsultaExtrato.consultar(codigoSTICotista, codigoSTI,
					dataFiltro);
		} else if (Canal.ARQUIVO == canal) {
			telaConsultaExtrato.fazerDownload(codigoSTICotista, codigoSTI,
					dataFiltro, usuario);
		}
	}

	private void consultaPosicao(Usuario usuario, String dataFiltro,
			String codigoSTI) {
		TelaHome telaHome = new TelaHome(usuario);
		TelaConsultaPosicao telaConsultaPosicao = (TelaConsultaPosicao) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_POSICAO);
		telaConsultaPosicao.fazerDownload(codigoSTI, dataFiltro, usuario);
	}

	private void consultaInfoAnbima(Usuario usuario, Canal canal,
			String dataFiltro, String codigoSTI) {
		TelaHome telaHome = new TelaHome(usuario);
		TelaConsultaPLCota telaConsultaPLCota = (TelaConsultaPLCota) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_PL_COTA);
		if (Canal.PORTAL == canal) {
			telaConsultaPLCota.consultar(codigoSTI, dataFiltro);
		} else if (Canal.ARQUIVO == canal) {
			telaConsultaPLCota.fazerDownload(codigoSTI, dataFiltro, usuario,
					ConstantesTestes.PATH_DOWNLOAD_INFO,
					Operacao.CONSULTA_TRANSFERENCIA);
		}
	}

	private void consultarPlCota(Usuario usuario, Canal canal,
			String dataFiltro, String codigoSTI) {
		TelaHome telaHome = new TelaHome(usuario);
		TelaConsultaPLCota telaConsultaPLCota = (TelaConsultaPLCota) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_PL_COTA);
		if (Canal.PORTAL == canal) {
			telaConsultaPLCota.consultar(codigoSTI, dataFiltro);
		} else if (Canal.ARQUIVO == canal) {
			telaConsultaPLCota.fazerDownload(codigoSTI, dataFiltro, usuario,
					ConstantesTestes.PATH_DOWNLOAD_PL_COTA,
					Operacao.CONSULTA_TRANSFERENCIA);
		}
	}

}